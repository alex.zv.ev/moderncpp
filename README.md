## Код должен быть понятным даже без чтения комментариев

Поскольку обычно программисты не читают комментарии в коде, хорошей практикой является писать его так, чтобы были понятны намерения:

```cpp
for (int i = 0; i < v.size(); ++i) {
    if (v[i] == val) {
        index = i;
        break;
    }
}
```
Данный код находит индекс значения в векторе, но код ниже намного лучше. Он сразу даёт понять намерение программиста:
```cpp
auto p = find(begin(v), end(v), val); 
```
> Нужно стараться избегать кода, который дублирует функционал стандартной библиотеки.

Также следует соблюдать *const-correctness*, не только для ряда оптимизаций со стороны компилятора, но и для четкого понимания намерений читателем кода.

## Код должен четко показывать намерение
```cpp
gsl::index i = 0;
while (i < v.size()) {
    // ... do something with v[i] ...
}
```
Данный код проходит по массиву, но это не выражено явно. Более того, индекс `i` живет и за пределом области видимости цикла, что может привести читателя в заблуждение.

Лучше делать так:
```cpp
for (const auto& x : v) { /* do something with the value of x */ }
```

Это сразу даёт понимание того, что программист просто проходится по массиву. Более того, в коде явно указано, что массив модифицироваться не будет.

Ещё пример:
```cpp
draw_line(int, int, int, int);
```
Тут непонятно, функция принимает x1, y1, x2, y2, или может в другом порядке, или может это x, y, h, w. Вот так будет лучше:
```cpp
draw_line(Point, Point);
```
> Если функция принимает слишком много значений стандартых типов, то имеет смысл задуматься, понятно ли будет намерение читателю.

## Стараться писать максимально безопасный в плане типов код

Некоторые механизмы в языке могут проходить этап компиляции, но на этапе выполнения вызывать непредвиденные ошибки. Например:
* `union` - лучше использовать `variant`
* Приведение типов - минимизировать их использование, могут помочь шаблоны.
* `array decay`
* `narrowing conversions` - постараться минимизировать. 

## Не откладывать проверки этапа компиляции на этап выполнения
Некоторые ошибки могут быть обнаружены на этапе компиляции. Их, конечно, лучше там и отловить, например, с помощью `static_assert`. Так можно проверять, к примеру, размер в байтах переменной, чтобы избежать переполнения.

## Не допускать утечек памяти
Плохой пример:
```cpp
void f(char* name)
{
    FILE* input = fopen(name, "r");
    // ...
    if (something) return;   // bad: if something == true, a file handle is leaked
    // ...
    fclose(input);
}
```

Хороший пример ([RAII](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#Rr-raii)):
```cpp
void f(char* name)
{
    ifstream input {name};
    // ...
    if (something) return;   // OK: no leak
    // ...
}
```

По этой же причине так полезны, например, `std::lock_guard` и `std::unique_ptr`.

## Лучше работать с неизменяемыми данными, чем с изменяемыми
Неизменяемые данные не могут неожиданно измениться, они дают компилятору больше пространства для оптимизаций, с ними не бывает гонок (data races).

# Интерфейсы
## Интерфейсы должны быть явными
Плохой пример функции, работа которой зависит от глобальной переменной:
```cpp
int round(double d)
{
    return (round_up) ? ceil(d) : d;    // don't: "invisible" dependency
}
```
Такая функция может выдать разные результаты при одинаковых аргументах, что может быть неожиданностью для того, кто её использует.

## Избегать неконстантных глобальных переменных
```cpp
struct Data {
    // ... lots of stuff ...
} data;            // non-const data

void compute()     // don't
{
    // ... use data ...
}

void output()     // don't
{
    // ... use data ...
}
```
Много кто может модифицировать эту переменную, и уже не разберешься, что с ней происходит.

> При этом глобальные константы полезны, от них не нужно отказываться.

## Избегать синглтонов
/// TODO

## Интерфейсы должны быть строго типизированы
Не следует делать так:
```cpp
void pass(void* data);    // weak and under qualified type void* is suspicious
```
Тот, кто вызвал функцию, может привести что угодно к `void*`, что в итоге может спровоцировать ошибку.

Вместо этого лучше использовать шаблоны.

Еще пример:
```cpp
draw_rect(100, 200, 100, 500); // what do the numbers specify?

draw_rect(p.x, p.y, 10, 20); // what units are 10 and 20 in?
```
Тут непонятно, что для чего передается. `int` можно интерпретировать как размер или как координату. Поэтому лучше сделать так:
```cpp
void draw_rectangle(Point top_left, Point bottom_right);
void draw_rectangle(Point top_left, Size height_width);

draw_rectangle(p, Point{10, 20});  // two corners
draw_rectangle(p, Size{10, 20});   // one corner and a (height, width) pair
```
В этом коде сразу становится понятно, что мы передаём.

Еще пример:
```cpp
void blink_led(int time_to_blink) // bad -- the unit is ambiguous
{
    // ...
    // do something with time_to_blink
    // ...
}

void use()
{
    blink_led(2);
}
```
Непонятно, какие единицы измерения у time_to_blink. Лучше использовать типы из `std::chrono::duration`.

## Неиспользуемые параметры не должны иметь имени
```cpp
widget* find(const set<widget>& s, const widget& w, Hint);   // once upon a time, a hint was used
```

## Обычно лучше передавать T* или T& в функцию вместо умных указателей
```cpp
// accepts any int*
void f(int*);

// can only accept ints for which you want to transfer ownership
void g(unique_ptr<int>);

// can only accept ints for which you are willing to share ownership
void g(shared_ptr<int>);

// doesn't change ownership, but requires a particular ownership of the caller
void h(const unique_ptr<int>&);

// accepts any int
void h(int&);
```

Во-первых, при передаче умного указателя надо всё сделать так, чтобы объект случайно не уничтожился раньше времени. Во-вторых, `shared_ptr` медленный и лучше лишний раз его не передавать.

## Лучше возвращать из функции, чем записывать по &/* (F20)
A return value is self-documenting, whereas a & could be either in-out or out-only and is liable to be misused.

If you have multiple values to return, use a tuple or similar multi-member type.

Кроме случаев, когда объект будет дорого переместить.

## Для возврата нескольких значений, лучше использовать струкруту или кортеж.
```cpp
// BAD: output-only parameter documented in a comment
int f(const string& input, /*output only*/ string& output_data)
{
    // ...
    output_data = something();
    return status;
}

// GOOD: self-documenting
tuple<int, string> f(const string& input)
{
    // ...
    return {status, something()};
}
```
Так сразу будет понятно, где входные значения, а где выходные. И это удобно, если использовать `tuple` в связке с `std::tie` (C++11) или Structural binding (C++17)
```cpp
auto [ iter, success ] = my_set.insert("Hello");
```

## Null reference не бывает, поэтому если передаваемый объект может быть нулевым, то следует передавать по T* вместо T&.

## Если функция возвращает динамически созданный объект, то лучше возвращать `unique_ptr` вместо T*. (F26)
Применимо, когда нужно сделать вызывающего функцию новым владельцем объекта.

## Возвращать T& из оператора=. (F47)
```cpp
class Foo
{
 public:
    ...
    Foo& operator=(const Foo& rhs)
    {
      // Copy members.
      ...
      return *this;
    }
};
```

## Не возвращать std::move(local). (F48)
Это излишне, потому что есть copy ellision.

## Использовать лямбду вместо функции, когда нужно обращаться ко многим локальным переменным (F50)
```cpp
// writing a function that should only take an int or a string
// -- overloading is natural
void f(int);
void f(const string&);

// writing a function object that needs to capture local state and appear
// at statement or expression scope -- a lambda is natural
vector<work> v = lots_of_work();
for (int tasknum = 0; tasknum < max; ++tasknum) {
    pool.run([=, &v] {
        /*
        ...
        ... process 1 / max - th of v, the tasknum - th chunk
        ...
        */
    });
}
pool.join();
```

## Если есть выбор, то стандартные аргументы лучше перегрузки (F51)

## Не передавать по ссылке в лямбду локальные значения, если она не отработает здесь и сейчас
Вряд ли объект, переданный по ссылке доживёт до момента выполнения лямбды, если она запускается, например, в другом потоке.

## Не использовать `va_arg` (F55)
```cpp
int sum(...)
{
    // ...
    while (/*...*/)
        result += va_arg(list, int); // BAD, assumes it will be passed ints
    // ...
}

sum(3, 2); // ok
sum(3.14159, 2.71828); // BAD, undefined

template<class ...Args>
auto sum(Args... args) // GOOD, and much more flexible
{
    return (... + args); // note: C++17 "fold expression"
}

sum(3, 2); // ok: 5
sum(3.14159, 2.71828); // ok: ~5.85987
```

Альтернативы:
* перегрузка
* шаблоны
* `variant`
* `initializer_list`

## Избегать ненужных операторов условий
Примеры:
```cpp
// Bad: Deep nesting
void foo() {
    ...
    if (x) {
        computeImportantThings(x);
    }
}

// Bad: Still a redundant else.
void foo() {
    ...
    if (!x) {
        return;
    }
    else {
        computeImportantThings(x);
    }
}

// Good: Early return, no redundant else
void foo() {
    ...
    if (!x)
        return;

    computeImportantThings(x);
}
```

```cpp
// Bad: Unnecessary nesting of conditions
void foo() {
    ...
    if (x) {
        if (y) {
            computeImportantThings(x);
        }
    }
}

// Good: Merge conditions + return early
void foo() {
    ...
    if (!(x && y))
        return;

    computeImportantThings(x);
}
```

# Классы и иерархии классов

## Если данные имеют отношение друг к другу, лучше объединить их в структуру, чтобы сам код отражал связь. (С1)
```c++
void draw(int x, int y, int x2, int y2);  // BAD: unnecessary implicit relationships
void draw(Point from, Point to);          // better
```

## Использовать класс вместо структуры, если есть приватное поле / функция (С8)
Необходимо для читабельности

## Если класс владеет чем-то, лучше `unique_ptr` вместо `T*` или `T&`. (C32)
Нужно для того, чтобы было понятно, кто владеет объектом.

## Если класс имеет указатель на объект, которым владеет, нужно определить деструктор (С33)
Поскольку класс владеет объектом, он должен его сам удалить в своём деструкторе.
> Также тут нужно задуматься насчет конструкторов копирования, поскольку если указатель скопируется, то он может удалиться дважды.

## Конструктор должен полностью инициализировать объкет (C41)
Нужно, чтобы использующий класс был уверен, что объект можно использовать сразу после его создания.
```cpp
class X1 {
    FILE* f;   // call init() before any other function
    // ...
public:
    X1() {}
    void init();   // initialize f
    void read();   // read from f
    // ...
};

void f()
{
    X1 file;
    file.read();   // crash or bad read!
    // ...
    file.init();   // too late
    // ...
}
```

Если не получается, можно использовать Фабрику.

## Если в конструкторе что-то пошло не так, нужно выбросить исключение (C42)
Не следует делать всякие `is_valid()`.
```cpp
class X2 {
    FILE* f;
    // ...
public:
    X2(const string& name)
        :f{fopen(name.c_str(), "r")}
    {
        if (!f) throw runtime_error{"could not open" + name};
        // ...
    }

    void read();      // read from f
    // ...
};

void f()
{
    X2 file {"Zeno"}; // throws if file isn't open
    file.read();      // fine
    // ...
}
```

## Конструкторы с одним аргументом должны быть `explicit `(C46)
Нужно, чтобы избежать неожиданного приведения типов.
```cpp
class String {
public:
    String(int);   // BAD
    // ...
};

String s = 10;   // surprise: string of size 10
```

## В конструкторе инициализировать значения в порядке их объявления (С47)
Нужно, чтобы избежать ошибок и не запутаться.

Плохой пример:
```cpp
class Foo {
    int m1;
    int m2;
public:
    Foo(int x) :m2{x}, m1{++x} { }   // BAD: misleading initializer order
    // ...
};

Foo x(1); // surprise: x.m1 == x.m2 == 2
```

## оператор= должен работать корректно, если передать этот же объект. Аналогично с move assignment (C62, C65)
Если `x = x` меняет значение `x`, то это плохо, могут быть ошибки и утечки.

```cpp
class Foo {
    string s;
    int i;
public:
    Foo& operator=(const Foo& a);
    // ...
};

Foo& Foo::operator=(const Foo& a)   // OK, but there is a cost
{
    if (this == &a) return *this;
    s = a.s;
    i = a.i;
    return *this;
}
```

## Использовать `=delete` и `=default`, когда нужно, вместо своих реализаций (C80, C81)
Потому что компилятор всё равно сделает это быстрее и лучше.

## [Не вызывать виртуальные функции в конструкторах и деструкторах](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c82-dont-call-virtual-functions-in-constructors-and-destructors)
/// TODO не совсем понимаю
```cpp
class Base {
public:
    virtual void f() = 0;   // not implemented
    virtual void g();       // implemented with Base version
    virtual void h();       // implemented with Base version
    virtual ~Base();        // implemented with Base version
};

class Derived : public Base {
public:
    void g() override;   // provide Derived implementation
    void h() final;      // provide Derived implementation

    Derived()
    {
        // BAD: attempt to call an unimplemented virtual function
        f();

        // BAD: will call Derived::g, not dispatch further virtually
        g();

        // GOOD: explicitly state intent to call only the visible version
        Derived::g();

        // ok, no qualification needed, h is final
        h();
    }
};
```

## Если базовый класс используется как интерфейс, он должен быть абстрактным (C121)

## Виртуальные функции должны иметь что-то одно из (`virtual`, `override`, `final`) (C128)
Нужно для читабельности и для отлова ошибок на этапе компиляции. Может несоответствовать тип аргумента, или отсутствовать `const`, а компилятор так сможет об этом предупредить.

Плохой пример:
```cpp
struct B {
    void f1(int);
    virtual void f2(int) const;
    virtual void f3(int);
    // ...
};

struct D : B {
    void f1(int);        // bad (hope for a warning): D::f1() hides B::f1()
    void f2(int) const;  // bad (but conventional and valid): no explicit override
    void f3(double);     // bad (hope for a warning): D::f3() hides B::f3()
    // ...
};
```
Хороший пример:
```cpp
struct Better : B {
    void f1(int) override;        // error (caught): Better::f1() hides B::f1()
    void f2(int) const override;
    void f3(double) override;     // error (caught): Better::f3() hides B::f3()
    // ...
};
```

## Все функции в интерфейсах должны быть чистыми виртуальными (C129)
Нужно, потому что со временем в иерархии становится сложно создавать и поддерживать конструкторы или функции

Плохой пример:
```cpp
class Shape {   // BAD, mixed interface and implementation
public:
    Shape();
    Shape(Point ce = {0, 0}, Color co = none): cent{ce}, col {co} { /* ... */}

    Point center() const { return cent; }
    Color color() const { return col; }

    virtual void rotate(int) = 0;
    virtual void move(Point p) { cent = p; redraw(); }

    virtual void redraw();

    // ...
private:
    Point cent;
    Color col;
};

class Circle : public Shape {
public:
    Circle(Point c, int r) : Shape{c}, rad{r} { /* ... */ }

    // ...
private:
    int rad;
};

class Triangle : public Shape {
public:
    Triangle(Point p1, Point p2, Point p3); // calculate center
    // ...
};
```

Хороший пример:
```cpp
class Shape {  // pure interface
public:
    virtual Point center() const = 0;
    virtual Color color() const = 0;

    virtual void rotate(int) = 0;
    virtual void move(Point p) = 0;

    virtual void redraw() = 0;

    // ...
};

class Circle : public Shape {
public:
    Circle(Point c, int r, Color c) : cent{c}, rad{r}, col{c} { /* ... */ }

    Point center() const override { return cent; }
    Color color() const override { return col; }

    // ...
private:
    Point cent;
    int rad;
    Color col;
};
```

## Избегать тривиальных геттеров и сеттеров (C131)
Вместо тривиального геттера и сеттера можно просто сделать переменную публичной.

Плохой пример:
```cpp
class Point {   // Bad: verbose
    int x;
    int y;
public:
    Point(int xx, int yy) : x{xx}, y{yy} { }
    int get_x() const { return x; }
    void set_x(int xx) { x = xx; }
    int get_y() const { return y; }
    void set_y(int yy) { y = yy; }
    // no behavioral member functions
};
```

Можно упростить этот класс до структуры:
```cpp
struct Point {
    int x {0};
    int y {0};
};
```

## Не делать функции виртуальными просто так (C132)
Это делает код медленнее и может вызвать путаницу.

## Не писать элементы `enum` капсом (E5)
Пример, где это будет ошибкой (конфликты с define):
```cpp
 // webcolors.h (third party header)
#define RED   0xFF0000
#define GREEN 0x00FF00
#define BLUE  0x0000FF

// productinfo.h
// The following define product subtypes based on color

enum class Product_info { RED, PURPLE, BLUE };   // syntax error
```

## Указывать тип перечисления только, когда нужно (E7)
```cpp
enum class Direction : char { n, s, e, w,
                              ne, nw, se, sw };  // underlying type saves space

enum class Web_color : int32_t { red   = 0xFF0000,
                                 green = 0x00FF00,
                                 blue  = 0x0000FF };  // underlying type is redundant
```

## Не присваивать лишний раз значения элементам перечислений (E8)
Так можно допустить ошибку, а еще при добавлении нового элемента может понадобиться переписывать значения.
Исключением являются случаи, когда значения нужны конкретно такие (например, это битмаски)
Можно присваивать первому элементу единицу.
```cpp
enum class Col1 { red, yellow, blue };
enum class Col2 { red = 1, yellow = 2, blue = 2 }; // typo
enum class Month { jan = 1, feb, mar, apr, may, jun,
                   jul, august, sep, oct, nov, dec }; // starting with 1 is conventional
enum class Base_flag { dec = 1, oct = dec << 1, hex = dec << 2 }; // set of bits
```

# Управление ресурсами
## Отдать предпочтение автоматическому управлению ресурсами, чем делать это вручную, соблюдать принцип RAII (R1)
Пример с ошибкой. Если произойдет ошибка при отправке и выбросится exception, то мьютекс обратно не разблокируется:
```cpp
void send(X* x, string_view destination)
{
    auto port = open_port(destination);
    my_mutex.lock();
    // ...
    send(port, x);
    // ...
    my_mutex.unlock();
    close_port(port);
    delete x;
}
```

Можно это исправить:
```cpp
void send(unique_ptr<X> x, string_view destination)  // x owns the X
{
    Port port{destination};            // port owns the PortHandle
    lock_guard<mutex> guard{my_mutex}; // guard owns the lock
    // ...
    send(port, x);
    // ...
} // automatically unlocks my_mutex and deletes the pointer in x
```
Тут мы используем `lock_guard`, который сам разлочит мьютекс даже при выброшенном исключении, потому что при исключении всегда вызываются деструкторы локальных переменных.

## Не выделять лишний раз память в куче, когда можно сделать это в стеке (R3)
Аллокация и деаллокация тоже занимает время. Более того, в следующем примере есть еще ошибка: если будет выброшено исключение, то `p` не удалится и будет утечка.

Плохой пример:
```cpp
void f(int n)
{
    auto p = new Gadget{n};
    // ...
    delete p;
}
```
Так что тут проще сделать так:
```cpp
void f(int n)
{
    Gadget g{n};
    // ...
}
```

## Не использовать `malloc`, `calloc` и `free`. (R10)
Они не работают с конструкторами и деструкторами и не дружат с `new/delete`.

## Избегать прямого использования `new` и `delete`. (R11)
Результат `new` обычно записывается в голый указатель, из-за чего может быть утечка памяти.

С `delete` тоже могут быть проблемы с несколькмим деаллокациями. Лучше оставить использование `delete` на разработчиков инструментов по управлению памятью.

Обычно `new`, `delete` можно заменить на `make_unique` или `make_shared`.

## Использовать `unique_ptr` и `shared_ptr`, чтобы явно указать владельца объекта. (R20)
```cpp
void f()
{
    X* p1 { new X };              // bad, p1 will leak
    auto p2 = make_unique<X>();   // good, unique ownership
    auto p3 = make_shared<X>();   // good, shared ownership
}
```

## Стараться использовать `unique_ptr` вместо `shared_ptr`, если не нужно разделять владение объектом. (R21)
Причина: `unique_ptr` быстрее и проще.

## Использовать `weak_ptr`, чтобы не закикливать `shared_ptr`. (R24)

# Выражения
## Не повторять код, когда это возможно (ES3)
```cpp
void func(bool flag)    // Bad, duplicated code.
{
    if (flag) {
        x();
        y();
    }
    else {
        x();
        z();
    }
}

void func(bool flag)    // Better, no duplicated code.
{
    x();

    if (flag)
        y();
    else
        z();
}
```

## Объявлять по одной переменной в строке. (ES10)
Это избавит от грамматических ошибок C++ и позволит написать более детальные комментарии на конце каждой строки.

Плохой пример:
```cpp
char *p, c, a[7], *pp[7], **aa[10];   // yuck!
```
А еще можно случайно не заметить неиницаилазированную переменную:
```
int a = 10, b = 11, c = 12, d, e = 14, f = 15;
```

Исключение - structural binding, который специально для этого предназначен.

## Объявлять переменную непосредственно перед её использованием (ES21)
Нужно для читабельности кода

## Использовать `{}` для инициализации (ES23)
Правила для инициализации "`{}`" проще, более однозначные и безопасные.
Использовать `=` можно только при уверенности, что нет сужающих приведений типов. Для арифметических операций `=` следует использовать с `auto`.

Пример:
```cpp
int x {f(99)};
int y = x;
vector<int> v = {1, 2, 3, 4, 5, 6};
```

Такая инициализация запрещает сужающие приведения типов.
```cpp
int x {7.9};   // error: narrowing
int y = 7.9;   // OK: y becomes 7. Hope for a compiler warning
```

## Помечать переменную `const` или `constexpr`, если не собирамся её модифицировать. (ES25)
Это сделает код более понятным и однозначным, возможно компилятор что-то оптимизирует.
```cpp
void f(int n)
{
    const int bufmax = 2 * n + 2;  // good: we can't change bufmax by accident
    int xmax = n;                  // suspicious: is xmax intended to change?
    // ...
}
```

## Если у переменной сложная инициализация, то её всё еще нужно пометить `const`, а инициализировать через лямбду. (ES28)
Плохой пример (x не константа, хотя по задумке дальше её не надо модифицировать):
```cpp
widget x;   // should be const, but:
for (auto i = 2; i <= N; ++i) {          // this could be some
    x += some_obj.do_something_with(i);  // arbitrarily long code
}                                        // needed to initialize x
// from here, x should be const, but we can't say so in code in this style
```

Хороший пример:
```cpp
const widget x = [&] {
    widget val;                                // assume that widget has a default constructor
    for (auto i = 2; i <= N; ++i) {            // this could be some
        val += some_obj.do_something_with(i);  // arbitrarily long code
    }                                          // needed to initialize x
    return val;
}();
```

## Не использовать макросы для "функций" или констант (ES31)
Причина: частое возникновение багов по этой причине. Проблемы с областью видимости. Возможные проблемы с передачей аргументов.

Плохой пример:
```cpp
#define PI 3.14
#define SQUARE(a, b) (a * b)
```

Можно исправить это:
```cpp
constexpr double pi = 3.14;
template<typename T> T square(T a, T b) { return a * b; }
```

## Называть макросы капсом. (ES32)
Нужно, чтобы можно было отличить от не макросов.
```cpp
#define forever for (;;)   /* very BAD */

#define FOREVER for (;;)   /* Still evil, but at least visible to humans */
```

## Избегать макросов с короткими или тривиальными названиями. (ES33)
Рано или поздно такой макрос начнет кофликтовать с другим такми же.
```cpp
#define MYCHAR        /* BAD, will eventually clash with someone else's MYCHAR*/

#define ZCORP_CHAR    /* Still evil, but less likely to clash */
```

## Избегать сложных выражений (ES40)
Могут возникать ошибки из-за путаницы.
```cpp
// bad: assignment hidden in subexpression
while ((c = getc()) != -1)

// bad: two non-local variables assigned in sub-expressions
while ((cin >> c1, cin >> c2), c1 == c2)

// better, but possibly still too complicated
for (char c1, c2; cin >> c1 >> c2 && c1 == c2;)

// OK: if i and j are not aliased
int x = ++i + ++j;

// OK: if i != j and i != k
v[i] = v[j] + v[k];

// bad: multiple assignments "hidden" in subexpressions
x = a + (b = f()) + (c = g()) * 7;

// bad: relies on commonly misunderstood precedence rules
x = a & b + c * d && e ^ f == 7;

// bad: undefined behavior
x = x++ + x++ + ++x;
```
```cpp
x = k * y + z;             // OK

auto t1 = k * y;           // bad: unnecessarily verbose
x = t1 + z;

if (0 <= x && x < max)   // OK

auto t1 = 0 <= x;        // bad: unnecessarily verbose
auto t2 = x < max;
if (t1 && t2)            // ...
```

## Если есть сомнения насчет приоритета операторов, использовать `()`. (ES41)
Причина: читаемость и избегание ошибок. Читатель кода может не помнить всю таблицу приоритетов операторов.
```cpp
const unsigned int flag = 2;
unsigned int a = flag;

if (a & flag != 0)  // bad: means a&(flag != 0)
```
```cpp
if ((a & flag) != 0)  // OK: works as intended
```

При этом не следует переусердствовать, тут и так ясен приоритет:
```cpp
if (a < 0 || a <= max) {
    // ...
}
```